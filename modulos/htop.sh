#!/bin/bash
clear
echo -e "\E[44;1;37m             HTOP           \E[0m"
echo ""
echo -e "\e[0;31mCOM ESTA FERRAMENTA VOCÊ VERIFICA TODOS OS PROCESSO DO SISTEMA\e[0m"
echo -e "" 
echo -e "\033[0;34mATENCÃO: \033[1;34m A INSTALAÇÃO PODE DEMORAR UNS SEGUNDOS\033[0m"
echo -e "" 
apt-get install htop > /dev/null 2>&1
sleep 4s
echo -e "\033[1;31mPARA SAIR PRESSIONE CTLR+C \e[0m"
sleep 5s
echo -e "\e[0;31m➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖\e[0m"

htop
