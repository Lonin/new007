#!/bin/bash
clear
echo -e "\E[44;1;37m             LIBERAR FIREWALL           \E[0m"
echo ""
echo -e "\033[1;31m• \033[1;33mPORTAS A SER LIBERADAS\033[0m"
echo -e "\033[1;31m• \033[1;33mTCP: 80,443,1194,7505,8088,8088,444,8799,8000\033[0m"
echo -e "\033[1;31m• \033[1;33mUDP: 7300,7200,7100\033[0m"
echo ""
read -p "Deseja Prosseguir? sim (y) não (n): " yn
    if [[ "$yn" =~ 'y' ]]; then continue; fi
    if [[ "$yn" =~ 'n' ]]; then exit; fi

sudo apt-get update
 
sudo apt install firewalld 

sudo firewall-cmd --zone=public --permanent --add-port=1194/tcp

sudo firewall-cmd --zone=public --permanent --add-port=7505/tcp
 
sudo firewall-cmd --zone=public --permanent --add-port=80/tcp

sudo firewall-cmd --zone=public --permanent --add-port=443/tcp

sudo firewall-cmd --zone=public --permanent --add-port=8088/tcp

sudo firewall-cmd --zone=public --permanent --add-port=8000/tcp

sudo firewall-cmd --zone=public --permanent --add-port=8080/tcp

sudo firewall-cmd --zone=public --permanent --add-port=444/tcp

sudo firewall-cmd --zone=public --permanent --add-port=8799/tcp

sudo firewall-cmd --zone=public --permanent --add-port=7300/udp

sudo firewall-cmd --zone=public --permanent --add-port=7200/udp

sudo firewall-cmd --zone=public --permanent --add-port=7100/udp
 
sudo firewall-cmd --reload 
 
sudo firewall-cmd --zone=public --list-ports
