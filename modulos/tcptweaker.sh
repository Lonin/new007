#!/bin/bash
tput setaf 7 ; tput setab 4 ; tput bold ; printf '%35s%s%-20s\n' "TCP Tweaker 1.1" ; tput sgr0
if [[ `grep -c "^#PH56" /etc/sysctl.conf` -eq 1 ]]
then
	echo ""
	echo -e "\033[1;33mAs configurações de rede TCP Tweaker já foram adicionadas no sistema!\033[0m"
	sleep 2
	echo ""
	read -p "(*) Deseja remover as configurações do TCP Tweaker? [s/n]: " -e -i n resposta0
	if [[ "$resposta0" = 's' ]]; then
		grep -v "^#PH56
net.ipv4.tcp_window_scaling = 1
net.core.rmem_max = 16777216
net.core.wmem_max = 16777216
net.ipv4.tcp_rmem = 4096 87380 16777216
net.ipv4.tcp_wmem = 4096 16384 16777216
net.ipv4.tcp_low_latency = 1
net.ipv4.tcp_slow_start_after_idle = 0" /etc/sysctl.conf > /tmp/syscl && mv /tmp/syscl /etc/sysctl.conf
sysctl -p /etc/sysctl.conf > /dev/null
		echo ""
		echo -e "\033[1;33mAs configurações de rede do TCP Tweaker foram removidas com sucesso.\033[0m"
		sleep 3
		clear
		menu
		echo ""
	exit
	else 
		echo ""
		exit
	fi
else

    echo ""
	echo ""
	echo -e "\033[1;31m• \033[1;33mEste é um script experimental. Use por sua conta e risco!\033[0m"
	echo -e "\033[1;31m• \033[1;33mEste script irá alterar algumas configurações de rede\033[0m"
	echo -e "\033[1;31m• \033[1;33mDo sistema para reduzir a latência e melhorar a velocidade.\033[0m"
	echo ""
	echo ""
	read -p  "(*) Deseja continua com a instalação? [s/n]:  " -e -i n resposta
	if [[ "$resposta" = 's' ]]; then
	echo ""
	echo -e "\033[1;31m• \033[1;33mModificando as seguintes configurações:\033[0m"
	sleep 3
	echo " " >> /etc/sysctl.conf
	echo "#PH56" >> /etc/sysctl.conf
echo "net.ipv4.tcp_window_scaling = 1
net.core.rmem_max = 16777216
net.core.wmem_max = 16777216
net.ipv4.tcp_rmem = 4096 87380 16777216
net.ipv4.tcp_wmem = 4096 16384 16777216
net.ipv4.tcp_low_latency = 1
net.ipv4.tcp_slow_start_after_idle = 0" >> /etc/sysctl.conf
echo ""
sysctl -p /etc/sysctl.conf
		echo ""
		echo -e "\033[1;33mAs configurações de rede do TCP Tweaker foram adicionadas com sucesso.\033[0m"
		sleep 3
		echo ""
	else
		echo ""
		echo -e "\033[1;33mA instalação foi cancelada pelo usuário!\033[0m"
		sleep 3
		echo ""
	fi
fi
exit
